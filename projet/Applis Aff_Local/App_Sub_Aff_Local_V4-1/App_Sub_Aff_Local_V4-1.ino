//----INCLUDES-----

//Pour l'afficheur LED
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

//Pour récupérer le fichier de config dans la SPIFF
#include "FS.h"

//Pour récupérer les messages aux format JSON
#include <ArduinoJson.h>

//Pour le WIFI
#include <ESP8266WiFi.h>    //Pour connexion au wifi

//Pour le broker MQTT
#include <PubSubClient.h>   //Pour connexion au broker MQTT


//CONSTANTES / VARIABLES / INSTANCES

//FICHIER DE CONFIG
//Chemin dans la spiff du fichier de conf
#define PATH_CONFIG "/ficheConfTest.json"  

//Taille allouée pour le fichier de config 
#define TAILLE_ALLOCATION 2800

File monFichierConf;        //Fichier de configuration
String contenuFichierConf;  //fichier de conf ss forme d'un String

DynamicJsonDocument jsonDocConf (TAILLE_ALLOCATION);    // Création de l’objet « JsonDocument » 
JsonObject objCfgJson;          //refercence sur le document json
DeserializationError error;     //Pour traitement des erreurs

//WIFI 
//const char* ssid = "tpsnir";   //Identifiant Wifi
//const char* wifiPassword = "t2p0s2n0";  // Mot de passe WiFi
const char* ssid;   //Identifiant Wifi
const char* wifiPassword;  // Mot de passe WiFi
WiFiClient wifiClient;    //Client Wifi

//MQTT
//char* mqttServer = "172.16.129.22";   //IP serveur MQTT
const char* mqttServer;   //IP serveur MQTT
int mqttPort;            //Port d'envoi MQTT 
const char* mqttUser;     // User MQTT (si il n'y en a pas, mettre NULL)  
const char* mqttPassword;     //Mot de passe (si il n'y en a pas, mettre NULL)

//signature de la fonction de callback
void callback(const char* topic, byte* payload, unsigned int length);

PubSubClient client;    //Client MQTT  (configuré plus tard)

//RS232


//TOPIC
const char* T_AFF = "afficheursLocaux";


//Code d'erreur
int codErr;

//LUMINOSITE (afficheur)
#define BRIGHTNESS 75

//MATRIX (afficheur)
#define MATRIX_WIDTH 32      //Largeur en pixel de l'afficheur
#define MATRIX_HEIGHT 8      //Hauteur en pixel de l'afficheur
#define PIN_AFF D6       //Broche utilisée pour l'afficheur

//création et configuration de la matrix (panneau d'affichage)
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(MATRIX_WIDTH, MATRIX_HEIGHT, PIN_AFF,
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800);

//tableau avec différentes couleur utilisable (R, G, B)
const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };

//position sur le panneau led
int posX = 2;   //Position (abscisse) du message (ici, il est un peu décalé vers la droite)
int posY = 1;   //Position (ordonné) du message (ici, il est un peu décalé vers le bas)

/*
//nombre de place à afficher
String nbPlacesToAff = "";
String nomEtage = "";
*/

//Signature de la fonction pour calculer le nombre d'etage
//qui devra etre afficher sur le panneau d'affichage
//int CalculNbEtageToAff(JsonObject prmObjCfgJson);





void setup() {

/*-----CONNEXION SERIE-----*/  
  //démarage connexion serie
  Serial.begin(115200); 
  Serial.println("Bonjour");
  
/*-------RECUP FICHIER CONF-------*/
  codErr = SPIFFS.begin();  //on monte le système de fichier 
   
  if (codErr == 1) {        //SI le fichier existe:
    
    Serial.println("Ouverture du fichier...");
    
    //on l'ouvre en mode lecture et on le stocke dans "monFichierConf"
    monFichierConf = SPIFFS.open(PATH_CONFIG, "r");

    //On stocke le contenu du fichier dans un String
    contenuFichierConf = monFichierConf.readString();

    //Affichage du contenu du fichier de conf
    Serial.println(contenuFichierConf);

    //Fermeture du fichier
    monFichierConf.close();
  }

  //Deserialisation du contenu du fichier (String -> JSON)
  error = deserializeJson(jsonDocConf, contenuFichierConf);

  if (error) {    //SI il y a une erreur:

    //Affichage de l'erreur
    Serial.println("Erreur de lecture du fichier");
    Serial.println(error.c_str());
    
  }else { //SI il n'y a pas d'erreur:

    //On récupère le fichier au format JSON sour forme d'objet JSON 
    objCfgJson = jsonDocConf.as<JsonObject>();

    //L'objet JSON est pret à etre utilisé
    // initialisation des variables   
    ssid = objCfgJson["ssid"];
    wifiPassword = objCfgJson["cleWifi"]; 
    mqttServer = objCfgJson["addServMQTT"]; 
    mqttPort = objCfgJson["portMQTT"]; 
    mqttUser = objCfgJson["UserMQTT"]; 
    mqttPassword = objCfgJson["pwdUserMQTT"];

    Serial.println(ssid);
    Serial.println(wifiPassword);
    Serial.println(mqttServer);
    Serial.println(mqttPort);
    Serial.println(mqttUser);  
    Serial.println(mqttPassword);
    }
    
    Serial.print("nombre d'etage: "); 
    Serial.println(ssid);
  
  
  
/*-----CONFIG PANNEAU LED-----*/
  //préparation des données pour la sortie
  matrix.begin();           

  //Mise a jour de la luminosité des leds
  matrix.setBrightness(BRIGHTNESS);

  //Mise à jour de la couleur du texte affiché sur le panneau 
  matrix.setTextColor(colors[0]);       //Rouge

  //on positionne le premier caractère aux coordonées suivante
  matrix.setCursor(posX, posY);  

  //initialisation PIN RS23

/*-----AFFICHAGE ADRESSE MAC-----*/ 
WiFi.mode(WIFI_STA);    //pour mettre en mode Station (cad: pas en point d'accès)
Serial.println(WiFi.macAddress());  //affichage de l'adresse MAC

/*-----CONFIG WIFI-----*/  

  // Démarrage de la connection WIFI  
  WiFi.begin(ssid, wifiPassword);
  Serial.print("Connecting");
  
  // on attend d’être connecté  au WiFi avant de continuer
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  //Connexion WiFi établie
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid); 

/*-----CONFIG MQTT-----*/
  //configuration de la connexion mqtt 
  //Serveur + port
  client.setServer(mqttServer, mqttPort);
  //fonction callback
  client.setCallback(callback);
  //client wifi 
  client.setClient(wifiClient);
  
  //connexion au serveur MQTT
  while (!client.connected()) {
    Serial.println("Connecting to Edge Gateway ...");
    if (client.connect("EspClient", mqttUser, mqttPassword)){
      Serial.println("Connected");  
    }
  }
     

  //abonnement au topic pour afficheur 
  client.subscribe(T_AFF);

  //initialisation du codErr
  codErr = client.state();
}







void loop() {
  client.loop();      

}





//-----------------Fonction de CallBack-------------------------
void callback(const char* topic, byte* payload, unsigned int length) {

  //objet qui contiendra le message JSON recu de la passerelle edge parsé (deserialisé) 
  DynamicJsonDocument objParsed(200);

  /******************Pour une version anterieure******************
  int nbEtage = 0;    //nombre d'etage qui devra etre affiché sur l'afficheur 
  String nivToAff;    //differents niveau à afficher
 *******************************************************************/
  


  //nom de l'etage du parking
  //const char* nomEtage = objCfgJson["etageToAff"][0];
  String nomEtage = objCfgJson["etageToAff"][0].as<String>();
  //String strNomEtage;
  
  //nombre de place disponible dans le parking
  int nbPlacesDispo;

  // converti le topic en string pour faciliter le travail
  String topicStr = topic; 

  //variable pour stocker le message recu (payload) ds une chaine de caractère (initialisé à vide)
  String messageRecu = "";


 /******************Pour une version anterieure******************
  //On determine le nombre d'etage à afficher
  nivToAff = objCfgJson["etageToAff"][nbEtage].as<String>();
  while ( nivToAff != "null") {
      nbEtage++;
      nivToAff = objCfgJson["etageToAff"][nbEtage].as<String>();
  }
*******************************************************************/

  //SI le message recu est publié sur le topic T_AFF
  if(topicStr == T_AFF) {

    //Affichage console de la reception
    Serial.println("Reception...");
    Serial.print("Topic: ");
    Serial.println(topicStr);
    
    //on met les leds de l'écran en position éteinte
    matrix.fillScreen(0);

    //on repositionne le curseur au debut de la ligne
    matrix.setCursor(2, 1);

    //concaténation de la chaine de charactère recu dans la variable messageRecu
    for (int i = 0; i < length; i++) {
      messageRecu +=  char(payload[i]);
    }
    Serial.println("voici le test");
    Serial.println(messageRecu);

    //on deserialise le message Recu (au format JSON) dans objParsed 
    DeserializationError error = deserializeJson(objParsed, messageRecu);

    //
    
    //SI il y a des erreurs 
    if (error) {
      Serial.print("deserializeJson() failed: ");
      Serial.println(error.c_str());
      
    } else {      //SINON      

      //SI l'étage envoyé est celui de l'afficheur 
      Serial.print("nomEtage est:" );
      Serial.println(nomEtage);
      Serial.print("objParsed est: ");
      Serial.println(objParsed["nomEtage"].as<String>());
      if (nomEtage == objParsed["nomEtage"].as<String>()) {

        //on stocke le nombre de place disponible de l'etage correspondant
        nbPlacesDispo = objParsed["nbPlacesDispo"];  

        //Affichage console
        //strNomEtage = String(nomEtage);
        //Serial.println("nombre de place du " + strNomEtage);        
        Serial.println(nbPlacesDispo);

        //AFFICHAGE PANNEAU LED
        matrix.print(nbPlacesDispo);    //pour l'affichage sur le panneau LED
  
        //On affiche sur le panneau LED le nombre de place à afficher
        matrix.show(); 
      }   






      
/************************Pour une version anterieure**************************************
      //SI il n'y a qu'un etage a afficher:
      if(nbEtage == 1) {
        //on stocke le nombre de place disponible de l'etage correspondant
        nbPlacesDispo = objParsed["nbPlacesDispo"];  

        //Affichage console     
        Serial.println(nbPlacesDispo);

        //AFFICHAGE PANNEAU LED
        matrix.print(nbPlacesDispo);    //pour l'affichage sur le panneau LED
  
        //On affiche sur le panneau LED le nombre de place à afficher
        matrix.show();    
      } else {
             
      }

      
      //Pour chaque etage qui doit etre affiché 
      for (int i = 0; i<nbEtage; i++) {

        //On selectionne l'etage qui doit etre affiché
        nivToAff = objCfgJson["etageToAff"][nbEtage].as<String>();

        //on met les leds de l'écran en position éteinte
        matrix.fillScreen(0);

        //on repositionne le curseur au debut de la ligne
        matrix.setCursor(2, 1);

        

        //SI l'étage envoyé est celui de l'afficheur 
        if (nivToAff == objParsed["nomEtage"].as<String>()) {

          //on stocke le nombre de place disponible de l'etage correspondant
          nbPlacesDispo = objParsed["nbPlacesDispo"];  

          //Affichage console
          //strNomEtage = String(nomEtage);
          //Serial.println("nombre de place du " + strNomEtage);        
          Serial.println(nbPlacesDispo);

          //AFFICHAGE PANNEAU LED
          matrix.print(nbPlacesDispo);    //pour l'affichage sur le panneau LED
  
          //On affiche sur le panneau LED le nombre de place à afficher
          matrix.show(); 

          delay(1000);
        }
**********************************************************************************/
      

    }    
  }
}

//MODIFIER LE CODE
/*NOTE: LES CHAINES DE CARACTERES RETOURNEES PAR obJParsed DOIVENT ETRE STOCKE DANS UN const char*
ex:  //On stocke le nom de l'étage 
      const char* Etage = objParsed["nomEtage"];
      String stringNomEtage = String(Etage);
      (ref: testLibJSON)
*/

//RESTE A FAIRE 
/*
 * Gerer quel etage doit etre afficher 
*/
